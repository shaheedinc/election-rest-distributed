/*
  Created by
  Shaheed Ahmed Dewan Sagar (200401357)
  Anup Benjamin Gomes (200388845)
  For course CS-872
*/
package controller;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.*;
import common.Utils;
import model.Block;
import model.Blockchain;
import model.Candidate;
import model.Election;
import model.request.VoteRequest;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * Service class to operate all kinds of election activities
 */
public class ElectionService {

    private Firestore db;

    public ElectionService() {
        try {
            // initializing firestore db
            FileInputStream serviceAccount = new FileInputStream("serviceAccountKey.json");
            FirestoreOptions firestoreOptions =
                    FirestoreOptions.getDefaultInstance().toBuilder()
                            .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                            .setProjectId(Utils.FIREBASE_PROJECT_ID)
                            .build();

            this.db = firestoreOptions.getService();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // get the server url for a user
    public String getUserServer(String userId) throws ExecutionException, InterruptedException {
        DocumentReference docRef = db.collection("users").document(userId);
        ApiFuture<DocumentSnapshot> future = docRef.get();
        DocumentSnapshot document = future.get();

        if (document.exists()) {
            return document.getString("serverUrl");
        } else {
            return "";
        }
    }

    // used to fetch all elections in firestore collection
    public List<Election> getAllElections() throws ExecutionException, InterruptedException {

        ApiFuture<QuerySnapshot> query = this.db.collection("elections").get();

        QuerySnapshot querySnapshot = query.get();
        List<QueryDocumentSnapshot> documents = querySnapshot.getDocuments();
        List<Election> allElections = new ArrayList<>();
        for (QueryDocumentSnapshot document : documents) {
            Election election = document.toObject(Election.class);
            allElections.add(election);
            System.out.println("election: " + election.toString());
            System.out.println();
        }
        return allElections;
    }

    // used to get a single election from firestore collection
    public Election getElection(String electionId) throws ExecutionException, InterruptedException {

        ApiFuture<QuerySnapshot> query = this.db.collection("elections").get();

        QuerySnapshot querySnapshot = query.get();
        List<QueryDocumentSnapshot> documents = querySnapshot.getDocuments();
        Election returnElection = null;
        for (QueryDocumentSnapshot document : documents) {
            Election election = document.toObject(Election.class);
            if (election.getId().equalsIgnoreCase(electionId)) {
                returnElection = election;
                break;
            }
        }
        return returnElection;
    }

    // used to check if an election's result is published
    public boolean electionPublished(VoteRequest voteRequest) throws ExecutionException, InterruptedException {
        boolean isPublished = false;
        List<Election> allElections = this.getAllElections();
        for (Election election : allElections) {
            if (election.getId().equalsIgnoreCase(voteRequest.getElectionId())) {
                isPublished = election.isPublished();
                break;
            }
        }
        return isPublished;
    }

    // used to count votes of any published election
    public List<Candidate> countVotes(String electionId) throws ExecutionException, InterruptedException {
        Election election = this.getElection(electionId);
        Blockchain blockchain = this.getBlockChain(electionId);
        for (Block block : blockchain.getBlockchain()) {
            Candidate candidate = election.getCadidate(block.getVoteRequest().getCandidateId());
            if (candidate != null) {
                candidate.incrementVoteCount();
            }
        }
        return election.getCandidates();
    }

    // used to find the candidates of an election
    public Candidate getCandidateOfElection(String electionId, String candidateId) throws ExecutionException, InterruptedException {

        Candidate candidateToReturn = null;
        List<Election> elections = getAllElections();
        for (Election election : elections) {
            if (election.getId().equalsIgnoreCase(electionId)) {
                for (Candidate candidate : election.getCandidates()) {
                    if (candidate.getId().equalsIgnoreCase(candidateId)) {
                        candidateToReturn = candidate;
                        break;
                    }
                }
                if (candidateToReturn != null) {
                    break;
                }
            }
        }

        return candidateToReturn;
    }

    // used to submit a vote request to persist in blockchain
    public String vote(VoteRequest voteRequest) throws ExecutionException, InterruptedException {
        Blockchain blockchain = this.getBlockChain(voteRequest.getElectionId());
        String blockId = UUID.randomUUID().toString();
        Block block = new Block(
                blockId,
                voteRequest,
                blockchain.getBlockchain().get(blockchain.getBlockchain().size() - 1).getHash()
        );
        block.setHash(block.generateHash());
        blockchain.getBlockchain().add(block);

        ApiFuture<WriteResult> future = db.collection("blockchain").document(voteRequest.getElectionId()).set(blockchain);
        future.get();
        return blockId;
    }

    // used to check if a user's credentials are valid
    public boolean isValidUser(VoteRequest voteRequest) throws ExecutionException, InterruptedException {
        DocumentReference docRef = db.collection("users").document(voteRequest.getVoterId());
        ApiFuture<DocumentSnapshot> future = docRef.get();
        DocumentSnapshot document = future.get();

        if (document.exists()) {
            String dob = document.getString("dob");
            return dob != null && dob.equalsIgnoreCase(voteRequest.getDob());
        } else {
            return false;
        }
    }

    // used to get the candidate voted for of a vote
    public Candidate getVotedCandidate(VoteRequest voteRequest) throws ExecutionException, InterruptedException {
        String candidateId = getCandidateIdWithTransactionId(voteRequest);
        voteRequest.setCandidateId(candidateId);
        if (candidateId != null) {
            return getCandidateOfElection(voteRequest.getElectionId(), voteRequest.getCandidateId());
        } else {
            return null;
        }
    }

    // used to get candidate with transaction id to verify the vote
    private String getCandidateIdWithTransactionId(VoteRequest voteRequest) throws ExecutionException, InterruptedException {
        String candidateId = null;
        Blockchain blockchain = this.getBlockChain(voteRequest.getElectionId());

        for (Block block : blockchain.getBlockchain()) {
            if (block.getVoteRequest().getVoterId().equalsIgnoreCase(voteRequest.getVoterId())
                    && block.getId().equalsIgnoreCase(voteRequest.getTransactionId())) {
                candidateId = block.getVoteRequest().getCandidateId();
            }
        }
        return candidateId;
    }

    // used to check if the user is allowed to vote
    public boolean isVotable(VoteRequest voteRequest) throws ExecutionException, InterruptedException {
        Blockchain blockchain = this.getBlockChain(voteRequest.getElectionId());

        System.out.println("votableBlockchain:"+blockchain.getBlockchain());

        boolean isVotable = true;
        for (Block block : blockchain.getBlockchain()) {
            if (block.getVoteRequest().getVoterId().equalsIgnoreCase(voteRequest.getVoterId())) {
                isVotable = false;
            }
        }

        return isVotable;
    }

    // used to fetch the whole blockchain
    private Blockchain getBlockChain(String electionId) throws ExecutionException, InterruptedException {
        DocumentReference docRef = db.collection("blockchain").document(electionId);
        ApiFuture<DocumentSnapshot> future = docRef.get();
        DocumentSnapshot document = future.get();

        if (document.exists()) {
            System.out.println("Document:"+document.get("blockchain"));
            return document.toObject(Blockchain.class);
        } else {
            Block genesisBlock = Block.createGenesisBlock(electionId);
            List<Block> blocks = new ArrayList<>();
            blocks.add(genesisBlock);
            Blockchain blockchain = new Blockchain(electionId, blocks);
            ApiFuture<WriteResult> futureInsert = db.collection("blockchain").document(electionId).set(blockchain);
            futureInsert.get();
            return blockchain;
        }
    }

}
