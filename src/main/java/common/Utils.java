/*
  Created by
  Shaheed Ahmed Dewan Sagar (200401357)
  Anup Benjamin Gomes (200388845)
  For course CS-872
*/

package common;

import java.security.MessageDigest;

// static Util class to hold static values
public final class Utils {

    public static final String DEFAULT_SERVER_ADDRESS = "http://142.3.83.4:4567";
    public static final int DEFAULT_SERVER_PORT = 4567;

    public static final String FIREBASE_PROJECT_ID = "election-management-91ef9";

    // used to generate hash of a block
    public static String applySha256(String input){
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(input.getBytes("UTF-8"));
            StringBuilder hexString = new StringBuilder();
            for (byte aHash : hash) {
                String hex = Integer.toHexString(0xff & aHash);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        }
        catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

}
