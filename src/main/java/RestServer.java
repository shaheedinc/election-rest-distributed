/*
  Created by
  Shaheed Ahmed Dewan Sagar (200401357)
  Anup Benjamin Gomes (200388845)
  For course CS-872
*/
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import common.Utils;
import model.Candidate;
import model.Election;
import controller.ElectionService;
import model.request.VoteRequest;

import java.util.List;

import static spark.Spark.*;

/**
 * Spark rest server main class
 */
public class RestServer {

    private static ObjectMapper objectMapper = new ObjectMapper();

    private static ElectionService electionService = new ElectionService();

    public static void main(String[] args) {

        // setting our desired port for the server
        port(Utils.DEFAULT_SERVER_PORT);

        // allowing CORS actions
        options("/*",
                (request, response) -> {

                    String accessControlRequestHeaders = request
                            .headers("Access-Control-Request-Headers");
                    if (accessControlRequestHeaders != null) {
                        response.header("Access-Control-Allow-Headers",
                                accessControlRequestHeaders);
                    }

                    String accessControlRequestMethod = request
                            .headers("Access-Control-Request-Method");
                    if (accessControlRequestMethod != null) {
                        response.header("Access-Control-Allow-Methods",
                                accessControlRequestMethod);
                    }

                    return "OK";
                });

        before((request, response) -> response.header("Access-Control-Allow-Origin", "*"));

        // handling the http get request for elections
        get("/elections", (req, res) -> {
            List<Election> allElections = null;
            try {
                allElections = electionService.getAllElections();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (allElections != null && allElections.size() > 0) {
                res.status(200);
                return objectMapper.writeValueAsString(allElections);
            } else {
                res.status(404);
                return objectMapper.writeValueAsString("No active elections found!");
            }
        });

        // handling the http get request for designated server ip
        get("/server",  (req, res) -> {
            String userId = req.queryParams("userId");
            System.out.println("findme"+userId);
            if (userId.trim().equalsIgnoreCase("")) {
                userId = "Fl3N0wfhOZ1SozSVfGJc";
            }
            res.status(200);
            JsonObject jsonObject = new JsonObject();
            String serverUrl = electionService.getUserServer(userId);
            if (serverUrl.equalsIgnoreCase("")) {
                serverUrl = Utils.DEFAULT_SERVER_ADDRESS;
            }
            jsonObject.addProperty("serverUrl", serverUrl);
            return objectMapper.writeValueAsString(jsonObject.toString());
        });

        // handling the http post request to vote
        post("/vote", (req, res) -> {
            VoteRequest voteRequest = new Gson().fromJson(req.body(), VoteRequest.class);
            res.status(200);
            if (voteRequest.getVoterId().trim().equalsIgnoreCase("")) {
                voteRequest.setVoterId("unavailable");
            }
            JsonObject jsonObject = new JsonObject();
            // Checking if user credential is valid
            if (electionService.isValidUser(voteRequest)) {
                // Checking if user has already voted
                if (electionService.isVotable(voteRequest)) {
                    String blockId = electionService.vote(voteRequest);
                    jsonObject.addProperty("message", "Vote accepted. You can check your vote using transaction id = " + blockId);
                    jsonObject.addProperty("transactionId", blockId);
                } else {
                    jsonObject.addProperty("message", "Vote is already casted");
                }
            } else {
                jsonObject.addProperty("message", "Invalid User");
            }
            return objectMapper.writeValueAsString(jsonObject.toString());
        });

        // handling the http post request to verify vote
        post("/verifyvote", (req, res) -> {
            VoteRequest voteRequest = new Gson().fromJson(req.body(), VoteRequest.class);
            res.status(200);
            JsonObject jsonObject = new JsonObject();
            // Checking if user credential is valid
            if (electionService.isValidUser(voteRequest)) {
                // getting and returning the candidate user voted for
                Candidate candidate = electionService.getVotedCandidate(voteRequest);
                if (candidate != null) {
                    jsonObject.addProperty("message", "Vote verified");
                    jsonObject.addProperty("candidateName", candidate.getName());
                    jsonObject.addProperty("candidatePhotoUrl", candidate.getPhotoUrl());
                } else {
                    jsonObject.addProperty("message", "Could not find vote information");
                }
            } else {
                jsonObject.addProperty("message", "Invalid User");
            }
            return objectMapper.writeValueAsString(jsonObject.toString());
        });

        // handling the http post request for result of an election
        post("/result", (req, res) -> {
            VoteRequest voteRequest = new Gson().fromJson(req.body(), VoteRequest.class);
            res.status(200);
            JsonObject jsonObject = new JsonObject();

            // checking if result if published
            if (electionService.electionPublished(voteRequest)) {
                // Counting and returning vote counts including candidate information
                List<Candidate> candidates = electionService.countVotes(voteRequest.getElectionId());
                jsonObject.addProperty("candidates", candidates.toString());
            } else {
                jsonObject.addProperty("message", "Result is not published yet");
            }

            return objectMapper.writeValueAsString(jsonObject.toString());
        });
    }

}
