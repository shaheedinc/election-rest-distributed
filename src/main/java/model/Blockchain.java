/*
  Created by
  Shaheed Ahmed Dewan Sagar (200401357)
  Anup Benjamin Gomes (200388845)
  For course CS-872
*/
package model;

import java.util.List;

// Model class to map blockchain of firestore collection
public class Blockchain {

    private String electionId;
    private List<Block> blockchain;

    public Blockchain() {
    }

    public Blockchain(String electionId, List<Block> blockchain) {
        this.electionId = electionId;
        this.blockchain = blockchain;
    }

    public String getElectionId() {
        return electionId;
    }

    public void setElectionId(String electionId) {
        this.electionId = electionId;
    }

    public List<Block> getBlockchain() {
        return blockchain;
    }

    public void setBlockchain(List<Block> blockchain) {
        this.blockchain = blockchain;
    }

    @Override
    public String toString() {
        return "Blockchain{" +
                "electionId='" + electionId + '\'' +
                ", blockchain=" + blockchain +
                '}';
    }
}
