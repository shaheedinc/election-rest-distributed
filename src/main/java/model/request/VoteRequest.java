/*
  Created by
  Shaheed Ahmed Dewan Sagar (200401357)
  Anup Benjamin Gomes (200388845)
  For course CS-872
*/
package model.request;

// Model class to map vote request from client
public class VoteRequest {

    private String electionId;
    private String candidateId;
    private String voterId;
    private String dob;
    private String transactionId;

    public VoteRequest() {

    }

    public VoteRequest(String electionId, String candidateId, String voterId, String dob) {
        this.electionId = electionId;
        this.candidateId = candidateId;
        this.voterId = voterId;
        this.dob = dob;
    }

    public String getElectionId() {
        return electionId;
    }

    public void setElectionId(String electionId) {
        this.electionId = electionId;
    }

    public String getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }

    public String getVoterId() {
        return voterId;
    }

    public void setVoterId(String voterId) {
        this.voterId = voterId;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public String toString() {
        return "VoteRequest{" +
                "electionId='" + electionId + '\'' +
                ", candidateId='" + candidateId + '\'' +
                ", voterId='" + voterId + '\'' +
                ", dob='" + dob + '\'' +
                ", transactionId='" + transactionId + '\'' +
                '}';
    }
}
