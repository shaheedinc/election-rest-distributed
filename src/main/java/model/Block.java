/*
  Created by
  Shaheed Ahmed Dewan Sagar (200401357)
  Anup Benjamin Gomes (200388845)
  For course CS-872
*/
package model;

import common.Utils;
import model.request.VoteRequest;

import java.util.Objects;

// Model class to map block of firestore collection
public class Block {

    private String id;
    private VoteRequest voteRequest;
    private String previousHash;
    private String hash;

    // used to create the first block or genesis block of blockchain
    public static Block createGenesisBlock(String electionId) {
        Block genesisBlock = new Block(
                "0",
                new VoteRequest(electionId, "0", "0", "0"),
                "0");
        genesisBlock.setHash(genesisBlock.generateHash());
        return genesisBlock;
    }

    public Block() {

    }

    public Block(String id, VoteRequest voteRequest, String previousHash) {
        this.id = id;
        this.voteRequest = voteRequest;
        this.previousHash = previousHash;
    }

    public Block(String id, VoteRequest voteRequest, String previousHash, String hash) {
        this.id = id;
        this.voteRequest = voteRequest;
        this.previousHash = previousHash;
        this.hash = hash;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public VoteRequest getVoteRequest() {
        return voteRequest;
    }

    public void setVoteRequest(VoteRequest voteRequest) {
        this.voteRequest = voteRequest;
    }

    public String getPreviousHash() {
        return previousHash;
    }

    public void setPreviousHash(String previousHash) {
        this.previousHash = previousHash;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String generateHash() {
        return Utils.applySha256(
                this.id + this.previousHash + this.voteRequest.toString()
        );
    }

    @Override
    public String toString() {
        return "Block{" +
                "id='" + id + '\'' +
                ", voteRequest=" + voteRequest +
                ", previousHash=" + previousHash +
                ", hash=" + hash +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Block)) return false;
        Block block = (Block) o;
        return getPreviousHash() == block.getPreviousHash() &&
                getHash() == block.getHash() &&
                Objects.equals(getId(), block.getId()) &&
                Objects.equals(getVoteRequest(), block.getVoteRequest());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getVoteRequest(), getPreviousHash(), getHash());
    }
}
