/*
  Created by
  Shaheed Ahmed Dewan Sagar (200401357)
  Anup Benjamin Gomes (200388845)
  For course CS-872
*/
package model;

import com.google.cloud.Timestamp;

import java.util.List;

// Model class to map elections of firestore collection
public class Election {

    private String id;
    private String expireDate;
    private String name;
    private List<Candidate> candidates;
    private boolean published;

    public Election() {

    }

    public Election(String id, String expireDate, String name, List<Candidate> candidates) {
        this.id = id;
        this.expireDate = expireDate;
        this.name = name;
        this.candidates = candidates;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Candidate> getCandidates() {
        return candidates;
    }

    public Candidate getCadidate(String candidateId) {
        Candidate c = null;
        for (Candidate candidate : candidates) {
            if (candidate.getId().equalsIgnoreCase(candidateId)) {
                c = candidate;
                break;
            }
        }
        return c;
    }

    public void setCandidates(List<Candidate> candidates) {
        this.candidates = candidates;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    @Override
    public String toString() {
        return "Election{" +
                "id='" + id + '\'' +
                ", expireDate='" + expireDate + '\'' +
                ", name='" + name + '\'' +
                ", candidates=" + candidates +
                ", published=" + published +
                '}';
    }
}
